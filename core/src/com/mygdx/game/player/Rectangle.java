package com.mygdx.game.player;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class Rectangle {

    //Color
    private int R = 255;
    private int G = 0;
    private int B = 0;
    private int ALPHA = 0;
    //XY
    private int startX;
    private int startY;
//    private int actuallX;
//    private int actuallY;
    //Dimensions
    private int width = 40;
    private int height = 50;

    public ShapeRenderer getShapeRenderer() {
        return shapeRenderer;
    }

    public void setShapeRenderer(ShapeRenderer shapeRenderer) {
        this.shapeRenderer = shapeRenderer;
    }

    ShapeRenderer shapeRenderer;
    public Rectangle(){
        shapeRenderer = new ShapeRenderer();

    }
    public void draw(int x,int y){
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(R, G, B, ALPHA);
        shapeRenderer.rect(x, y, width, height + 10);
        shapeRenderer.end();
    }
    public int getStartX() {
        return startX;
    }

    public void setStartX(int startX) {
        this.startX = startX;
    }

    public int getStartY() {
        return startY;
    }

    public void setStartY(int startY) {
        this.startY = startY;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

}
